import { StyleSheet, Text, View, ImageBackground, Image, TouchableOpacity } from 'react-native';
import { StatusBar } from 'expo-status-bar';
import { Link } from 'expo-router';
import React, { useState, useEffect } from 'react';

export default function Home() {
    const [indexImg, setIndexImg] = useState(1);

    const getImageSource = () => {
        switch (indexImg) {
            case 1:
                return require('../assets/mainScreen/1.png');
            case 2:
                return require('../assets/mainScreen/2.png');
            case 3:
                return require('../assets/mainScreen/3.png');
            case 4:
                return require('../assets/mainScreen/4.png');
            case 5:
                return require('../assets/mainScreen/5.png');
            default:
                return require('../assets/mainScreen/1.png');
        }
    };

    useEffect(() => {
        const interval = setInterval(() => {
            setIndexImg((prevIndex) => {
                if (prevIndex === 5) {
                    return 1;
                }
                return prevIndex + 1;
            });
        }, 99999);
        return () => clearInterval(interval);
    }
        , []);

    return (
        <ImageBackground source={getImageSource()} style={styles.backgroundImage}>
            <View style={styles.container}>
                <View />
                <View style={styles.contentContainer}>
                    <Image source={require('../assets/icon.png')} style={styles.icon} />
                    <Text style={styles.text}>Yomiyah</Text>
                </View>
                <StatusBar style="auto" />
                <TouchableOpacity style={styles.button} onPress={() => console.log("Commencer ma méditation")}>
                    <Link href="/Meditation" style={styles.link}>
                        <Text style={styles.buttonText}>Commencer ma méditation</Text>
                    </Link>
                </TouchableOpacity>
            </View>
        </ImageBackground>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingBottom: 50,
    },
    contentContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 20,
    },
    icon: {
        width: 30,
        height: 30,
        marginRight: 10,
    },
    text: {
        fontSize: 20,
        fontWeight: 'bold',
        color: 'white',
    },
    backgroundImage: {
        flex: 1,
        resizeMode: 'cover',
        justifyContent: 'center',
    },
    button: {
        backgroundColor: '#000',
        paddingVertical: 12,
        paddingHorizontal: 20,
        borderRadius: 25,
    },
    buttonText: {
        color: 'white',
        fontSize: 16,
        fontWeight: 'bold',
        textTransform: 'uppercase',
    },
    link: {
        textDecorationLine: 'none',
    },
});
