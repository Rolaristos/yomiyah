import { StyleSheet, Text, View } from 'react-native'
import { Slot, Stack } from 'expo-router';


const RootLayer = () => {
  return (
    <Stack>
      <Stack.Screen name='index' options={{ headerShown: false }} />
    </Stack>
  )
}

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  }
})

import React from 'react';
import { SafeAreaProvider } from 'react-native-safe-area-context';

export default function App() {
    return (
        <SafeAreaProvider>
            <RootLayer />
        </SafeAreaProvider>
    );
}
