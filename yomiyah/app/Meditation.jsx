import { StyleSheet, Text, View } from 'react-native';
import React, { useState, useEffect } from 'react';
import bibleData from '../assets/data/bible.json';

const Meditation = () => {
  const [randomBook, setRandomBook] = useState(null);
  const [randomChapter, setRandomChapter] = useState(null);

  useEffect(() => {
    // Tirer un livre au hasard
    const randomBookIndex = Math.floor(Math.random() * bibleData.livres.length);
    const randomBookData = bibleData.livres[randomBookIndex];
    setRandomBook(randomBookData);

    // Tirer un chapitre au hasard pour ce livre
    const randomChapterNumber = Math.floor(Math.random() * randomBookData.chapitres) + 1;
    setRandomChapter(randomChapterNumber);
  }, []);

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Chapitre "aléatoire" à méditer</Text>
      <View style={styles.divider} />
      <View style={styles.content}>
        <Text style={styles.label}>Livre:</Text>
        <Text style={styles.text}>{randomBook && randomBook.nom}</Text>
      </View>
      <View style={styles.divider} />
      <View style={styles.content}>
        <Text style={styles.label}>Chapitre:</Text>
        <Text style={styles.text}>{randomChapter}</Text>
      </View>
      <View style={styles.divider} />
      <Text style={styles.reminder}>
        N'oubliez pas de prier ! Que le Saint-Esprit vous inspire dans votre méditation.
      </Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 20,
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 10,
    textAlign: 'center',
  },
  divider: {
    height: 1,
    backgroundColor: 'black',
    width: '100%',
    marginVertical: 10,
  },
  content: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 10,
  },
  label: {
    fontSize: 18,
    fontWeight: 'bold',
    marginRight: 10,
  },
  text: {
    fontSize: 18,
  },
  reminder: {
    marginTop: 20,
    fontSize: 16,
    textAlign: 'center',
  },
});

export default Meditation;
