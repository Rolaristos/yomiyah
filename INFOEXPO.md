› Using Expo Go
› Press s │ switch to development build

› Press a │ open Android
› Press w │ open web

› Press j │ open debugger
› Press r │ reload app
› Press m │ toggle menu
› Press o │ open project code in your editor

› Press ? │ show all commands

###### **Autres infos :**

Pour faire un système de route:

npx expo install expo-router react-native-safe-area-context react-native-screens expo-linking expo-constants expo-status-bar
