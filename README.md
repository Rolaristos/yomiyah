# Yomiyah - Projet

Projet pensé et développé par moi-même.

# Origine du projet

Yomiyah en hébreu (quotidien, le nom complet de l'application serait donc "Méditation quotidienne" -> "Hitbonenut yomiyah" mais je trouvais cela trop long) est une application que j'ai eu à cœur de créer pour m'aider dans ma méditation quotidienne de la Bible.

# En quoi consiste l'application ?

À l'heure où j'écris ce README, je n'ai prévu qu'une seule et simple fonctionnalité de l'application : appuyer sur un bouton qui va ensuite générer un livre de la Bible ainsi qu'un chapitre de la Bible de façon aléatoire. Se référer à la section " **Fonctionnalités implémentées** " pour observer les changements.

# Perspectives d'évolution ?

Il y en aura très certainement. Il s'agit ici de ma toute première application en React Native, donc tout va se faire au fur et à mesure que je vais découvrir les fonctionnalités possibles.

Je vois déjà la possibilité de stocker les chapitres déjà obtenus, la possibilité de mettre une heure ou plusieurs de rappels pour la méditation et aussi la possibilité, pourquoi pas, de sélectionner de façon aléatoire un verset à travers toute la Bible entière.

# Fonctionnalités implémentées

Voici à ce jour les fonctionnalités implémentées :


# Aide

Pour lancer/créer le projet :

- https://docs.expo.dev/get-started/create-a-project/#start-the-development-server
